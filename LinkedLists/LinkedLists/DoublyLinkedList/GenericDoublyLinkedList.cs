﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.DoublyLinkedList
{
    public class GenericDoublyLinkedList<Type>
    {
        GenericDoubleNode<Type> head;
        GenericDoubleNode<Type> tail;
        int size;

        public GenericDoublyLinkedList()
        {
            head = null;
            tail = null;
            size = 0;
        }

        public GenericDoublyLinkedList(Type node)
        {
            head = new GenericDoubleNode<Type>(node);
            tail = head;
            size = 1;
        }

        public void AddFirst(Type data)
        {
            //Create a new node and initialize with data
            GenericDoubleNode<Type> newHead = new GenericDoubleNode<Type>(data);

            if (head == null)
            {
                head = newHead;
                tail = newHead;
            }
            else
            {
                //As well as the pointer to the previous head
                newHead.next = head;
                head.prev = newHead;
                //The new node is now the current head
                head = newHead;
            }
            size++;

        }

        public void AddLast(Type data)
        {
            //Create a new Node and initialize
            GenericDoubleNode<Type> newNode = new GenericDoubleNode<Type>(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
                tail = newNode;
            }
            else
            {
                //Point the new tail's prev to the current tail
                newNode.prev = tail;
                //Point the current tail the new tail
                tail.next = newNode;
                //Set the new tail to the new node
                tail = newNode;
                //The new tail should already be pointing to null
            }
            //increment size of linked list
            size++;
        }

        public void AddAtIndex(Type data, int index)
        {
            //Index is outside of our bounds
            if (index < 0 || index >= size)
            {
                Console.WriteLine("Index is outside of our bounds!");
                return;
            }

            //Insert is at the head
            if(index == 0)
            {
                //Call existing method
                AddFirst(data);
                return;
            }

            //Insert is at the end
            if(index == size - 1)
            {
                //Call existing method
                AddLast(data);
                return;
            }


            //Index is somewhere in between
            //Counter to iterate through list
            int counter = 0;
            //Get the head for iteration through the list
            GenericDoubleNode<Type> current = head;
            //Initialize the new node to be inserted
            GenericDoubleNode<Type> insertNode = new GenericDoubleNode<Type>(data);

            //Iterate until we find the index previous to the one we're looking for
            while (counter != index - 1)
            {
                current = current.next;
                counter++;
            }

            //Set the next node of our new node to the node we're replacing
            insertNode.next = current.next;
            //Set the prev node of our new node to the current index we're at
            insertNode.prev = current;
            //Set the next node of our current index to the new node we're inserting
            current.next = insertNode;
            //Set the prev node of the node we're replacing to point to the node we're inserting
            insertNode.next.prev = insertNode;
            size++;
        }

        public void Remove(int index)
        {
            //Index is outside of our bounds
            if (index < 0 || index >= size)
            {
                return;
            }

            if(index == 0)
            {
                head = head.next;
                return;
            }

            int counter = 0;
            GenericDoubleNode<Type> current = head;

            while(current.next != null)
            {
                if(counter == index - 1)
                {
                    current.next.next.prev = current;
                    current.next = current.next.next;
                    size--;
                    break;
                }
                current = current.next;
                counter++;
            }
        }

        public Type GetLast()
        {
            return tail.data;
        }

        public void DisplayReverse()
        {
            GenericDoubleNode<Type> node = tail;

            while(node.prev != null)
            {
                Console.Write(node.data + "->");
                node = node.prev;
            }

            Console.Write(node.data + "->null");
        }

        public void Display()
        {
            GenericDoubleNode<Type> current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }
    }
}
