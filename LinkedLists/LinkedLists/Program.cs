﻿using System;
using LinkedLists.Nodes;
using LinkedLists.SinglyLinkedList;
using LinkedLists.SinglyLinkedListWithTail;
using LinkedLists.DoublyLinkedList;

namespace LinkedLists
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Loosely typed linked list
            //ObjectLinkedList objList = new ObjectLinkedList();
            //objList.AddLast(5);
            //objList.AddLast(10);
            //objList.AddLast(15);
            //objList.AddLast(20);
            //objList.AddFirst(1);
            //objList.DisplayNodes();
            //Console.WriteLine("\n\n");

            ////Strongly typed linked list of type int
            //IntLinkedList intList = new IntLinkedList(25);
            //intList.AddLast(9);
            //intList.AddLast(2);
            //intList.AddFirst(7);
            //intList.DisplayNodes();
            //Console.WriteLine("\n\n");

            ////Loosely typed linked list with tail
            //ObjectLinkedListWithTail objTailList = new ObjectLinkedListWithTail();
            //objTailList.AddLast("Last");
            //Console.WriteLine($"Tail: {objTailList.GetLast()}");
            //objTailList.AddLast("One");
            //Console.WriteLine($"Tail: {objTailList.GetLast()}");
            //objTailList.AddFirst("First");
            //Console.WriteLine($"Tail: {objTailList.GetLast()}");
            //objTailList.AddLast("Tail");
            //Console.WriteLine($"Tail: {objTailList.GetLast()}");
            //objTailList.DisplayNodes();
            //Console.WriteLine("\n");

            ////Strongly typed Generic linked list: any type can be passed in to this implementation
            GenericLinkedList<string> strList = new GenericLinkedList<string>("Init");
            strList.AddLast("Str");
            strList.AddFirst("Hello");
            strList.AddLast("world");
            strList.DisplayNodes();
            Console.WriteLine("\n");
            strList.Reverse();
            strList.DisplayNodes();

            ////Strongly typed Generic List with Tail
            //GenericLinkedListWithTail<int> genericIntList = new GenericLinkedListWithTail<int>(6);
            //genericIntList.AddFirst(8);
            //genericIntList.AddLast(0);
            //genericIntList.AddLast(2);
            //genericIntList.DisplayNodes(); //Current linked list: 8->6->0->2->null
            //Console.WriteLine("\n");
            //genericIntList.Remove(4);
            //genericIntList.DisplayNodes(); //8->6->2->null
            //Console.WriteLine($"\n\nTail: {genericIntList.GetLast()}\n\n");

            //Strongly typed Generic Doubly Linked List
            GenericDoublyLinkedList<int> doublyLinkedList = new GenericDoublyLinkedList<int>();
            doublyLinkedList.AddFirst(1);
            doublyLinkedList.AddLast(3);
            doublyLinkedList.AddLast(4);
            doublyLinkedList.AddFirst(8);
            //doublyLinkedList.Remove(2);
            doublyLinkedList.AddAtIndex(9, 2);
            doublyLinkedList.Display();
            Console.WriteLine("\n");
            doublyLinkedList.DisplayReverse();
        }
    }
}
