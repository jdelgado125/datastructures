﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.Nodes
{
    public class Node
    {
        public int data = -1;
        public Node next = null;

        public Node()
        {

        }

        public Node(int data)
        {
            this.data = data;
        }
    }
}
