﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.Nodes
{
    public class ObjectNode
    {
        public object data = null;
        public ObjectNode next = null;

        public ObjectNode()
        {

        }

        public ObjectNode(object data)
        {
            this.data = data;
        }
    }
}