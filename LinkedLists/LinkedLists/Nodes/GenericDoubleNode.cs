﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.Nodes
{
    public class GenericDoubleNode<Type>
    {
        public Type data;
        public GenericDoubleNode<Type> next;
        public GenericDoubleNode<Type> prev;

        public GenericDoubleNode()
        {

        }

        public GenericDoubleNode(Type data)
        {
            this.data = data;
        }
    }
}
