﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.Nodes
{
    public class GenericNode<Type>
    {
        public Type data;
        public GenericNode<Type> next;

        public GenericNode()
        {

        }

        public GenericNode(Type data)
        {
            this.data = data;
        }
    }
}
