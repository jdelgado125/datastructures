﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedListWithTail
{
    public class ObjectLinkedListWithTail
    {
        ObjectNode head = null;
        ObjectNode tail = null;

        public ObjectLinkedListWithTail(object head = null)
        {
            if (head != null)
            {
                this.head = new ObjectNode(head);
                tail = this.head;
            }
        }

        public void AddLast(object data)
        {
            //Create a new Node and initialize
            ObjectNode newNode = new ObjectNode(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
                //And the tail
                tail = newNode;
            }
            else
            {
                //Since we are now keeping track of the tail, we can set the current tail to point to the new tail
                tail.next = newNode;
                //And update the new tail to be the node being added
                tail = newNode;
            }
        }

        public void AddFirst(object data)
        {
            //Create a new node and initialize with data
            ObjectNode newHead = new ObjectNode(data);

            if(head == null)
            {
                //head and tail are set to new node if current head is null
                head = newHead;
                tail = newHead;
            }
            else
            {
                //As well as the pointer to the previous head
                newHead.next = head;
                //The new node is now the current head
                head = newHead;
            }
        }

        public void DisplayNodes()
        {
            ObjectNode current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }

        public object GetLast()
        {
            return tail.data;
        }
    }
}
