﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedListWithTail
{
    public class GenericLinkedListWithTail<Type>
    {
        GenericNode<Type> head = null;
        GenericNode<Type> tail = null;
        int size = 0;

        public GenericLinkedListWithTail()
        {
        }

        public GenericLinkedListWithTail(Type head = default)
        {
            if (head != null)
            {
                this.head = new GenericNode<Type>(head);
                tail = this.head;
            }
            size++;
        }

        public void AddLast(Type data)
        {
            //Create a new Node and initialize
            GenericNode<Type> newNode = new GenericNode<Type>(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
                tail = newNode;
                size++;
            }
            else
            {
                //Point the current tail the new tail
                tail.next = newNode;
                //Set the new tail to the new node
                tail = newNode;
                //increment size of linked list
                size++;
            }

        }

        public void AddFirst(Type data)
        {
            //Create a new node and initialize with data
            GenericNode<Type> newHead = new GenericNode<Type>(data);
            if(head == null)
            {
                head = newHead;
                tail = newHead;
            }
            else
            {
                //As well as the pointer to the previous head
                newHead.next = head;
                //The new node is now the current head
                head = newHead;
            }
            size++;

        }

        public void Remove(int index)
        {
            int counter = 0;

            //Index is outside of our bounds
            if (index < size || index >= size)
            {
                return;
            }

            GenericNode<Type> current = head;

            while(counter != index - 1)
            {
                current = current.next;
                counter++;
            }

            current.next = current.next.next;
            size--;
        }

        public Type GetLast()
        {
            return tail.data;
        }

        public void DisplayNodes()
        {
            GenericNode<Type> current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }
    }
}
