﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedListWithTail
{
    public class IntLinkedListWithTail
    {
        Node head = null;
        Node tail = null;

        public IntLinkedListWithTail(int head = -1)
        {
            if (head != -1)
            {
                this.head = new Node(head);
                tail = this.head;
            }
        }

        public void AddLast(int data)
        {
            //Create a new Node and initialize
            Node newNode = new Node(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
                //And the tail
                tail = newNode;
            }
            else
            {
                //Since we are now keeping track of the tail, we can set the current tail to point to the new tail
                tail.next = newNode;
                //And update the new tail to be the node being added
                tail = newNode;
            }
        }

        public void AddFirst(int data)
        {
            //Create a new node and initialize with data
            Node newHead = new Node(data);

            if(head == null)
            {
                //head and tail are set to new node if current head is null
                head = newHead;
                tail = newHead;
            }
            else
            {
                //As well as the pointer to the previous head
                newHead.next = head;
                //The new node is now the current head
                head = newHead;
            }
        }

        public void DisplayNodes()
        {
            Node current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }

        public int GetLast()
        {
            return tail.data;
        }
    }
}
