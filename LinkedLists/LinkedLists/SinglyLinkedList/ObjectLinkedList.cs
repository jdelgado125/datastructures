﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedList
{
    public class ObjectLinkedList
    {
        ObjectNode head = null;

        public ObjectLinkedList(object head = null)
        {
            if (head != null)
            {
                this.head = new ObjectNode(head);
            }
        }

        public void AddLast(object data)
        {
            //Create a new Node and initialize
            ObjectNode newNode = new ObjectNode(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
            }
            else
            {
                //Set the current node to the beginning of the List
                ObjectNode current = head;
                //Find the last node, by checking the value of the next node
                while (current.next != null)
                {
                    current = current.next;
                }

                //last element in the list points to null; set next to the new node
                current.next = newNode;
            }

        }

        public void AddFirst(object data)
        {
            //Create a new node and initialize with data
            ObjectNode newHead = new ObjectNode(data);
            //As well as the pointer to the previous head
            newHead.next = head;
            //The new node is now the current head
            head = newHead;

        }

        public void DisplayNodes()
        {
            ObjectNode current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }
    }
}
