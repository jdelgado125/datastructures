﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedList
{
    public class IntLinkedList
    {
        Node head = null;

        //Constructor with optional initializer
        public IntLinkedList(int head = -1)
        {
            
            if(head != -1)
            {
                this.head = new Node(head);
            }
        }

        public void AddLast(int data)
        {
            //Initialize the new node that is to be added
            Node newNode = new Node(data);

            //If the head is still null, then our new node becomes the head
            if(head == null)
            {
                head = newNode;
            }
            else
            {
                //We need to find the last node in the list
                //Start from the head so we can iterate through
                Node current = head;
                //If the node is pointing to another node, we are not at the end
                while(current.next != null)
                {
                    //Get the next node as our current node
                    current = current.next;
                }

                //Out of the loop, so this node points to null == last node
                //Set the empty spot to the new node
                current.next = newNode;
            }
        }

        public void AddFirst(int data)
        {
            //Initialize new node to be added
            Node newHead = new Node(data);

            //Set the next pointer to the current head (can be null)
            newHead.next = head;

            //Set the new node as the new head
            head = newHead;
        }

        public void DisplayNodes()
        {
            //Get the current head so we can iterate through list
            Node current = head;

            //Iterate until we find the last node that point to null
            while (current.next != null)
            {
                //Print the value of the node before we move on to the next node
                Console.Write(current.data + "->");
                //Get the next node
                current = current.next;
            }

            //Print the last node
            Console.Write(current.data + "->null");
        }
    }
}
