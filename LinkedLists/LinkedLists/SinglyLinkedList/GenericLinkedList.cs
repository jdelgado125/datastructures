﻿using System;
using LinkedLists.Nodes;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists.SinglyLinkedList
{
    public class GenericLinkedList<Type>
    {
        GenericNode<Type> head = null;
        int size = 0;

        public GenericLinkedList()
        {
        }

        public GenericLinkedList(Type head = default)
        {
            if (head != null)
            {
                this.head = new GenericNode<Type>(head);
            }
            size++;
        }

        public void AddLast(Type data)
        {
            //Create a new Node and initialize
            GenericNode<Type> newNode = new GenericNode<Type>(data);

            //If there are no nodes, then we insert at the head
            if (head == null)
            {
                //Head is currently empty, so our new node is now the head
                head = newNode;
                size++;
            }
            else
            {
                //Set the current node to the beginning of the List
                GenericNode<Type> current = head;
                //Find the last node, by checking the value of the next node
                while (current.next != null)
                {
                    current = current.next;
                }

                //last element in the list points to null; set next to the new node
                current.next = newNode;
                size++;
            }

        }

        public void AddFirst(Type data)
        {
            //Create a new node and initialize with data
            GenericNode<Type> newHead = new GenericNode<Type>(data);
            //As well as the pointer to the previous head
            newHead.next = head;
            //The new node is now the current head
            head = newHead;
            size++;

        }

        public void Remove(int index)
        {
            int counter = 0;

            //Index is outside of our bounds
            if (index < size || index >= size)
            {
                return;
            }

            GenericNode<Type> current = head;

            while(counter != index - 1)
            {
                current = current.next;
                counter++;
            }

            current.next = current.next.next;
            size--;
        }

        public void DisplayNodes()
        {
            GenericNode<Type> current = head;
            while (current.next != null)
            {
                Console.Write(current.data + "->");
                current = current.next;
            }

            Console.Write(current.data + "->null");
        }

        public void Reverse()
        {
            GenericNode<Type> prev = head, next = null, tmp;

            while(prev != null)
            {
                tmp = prev.next;
                prev.next = next;
                next = prev;
                prev = tmp;
            }

            head = next;
        }
    }
}
