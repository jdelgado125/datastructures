﻿using System;

namespace Graph
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph(6);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 3);
            graph.AddEdge(2, 4);
            graph.AddEdge(3, 1);
            graph.AddEdge(5, 6);
            graph.BFS(1);
            graph.DFS(1);
        }
    }
}
