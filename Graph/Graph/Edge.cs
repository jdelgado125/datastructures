﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Graph
{
    class Edge
    {
        public Vertex V1 { get; set; }
        public Vertex V2 { get; set; }
    }
}
