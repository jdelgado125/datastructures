﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Graph
{
    class Graph
    {
        private int count = 0;
        private List<int>[] adjacent = null;
        private bool[] visited = null;

        public Graph(int size)
        {
            count = size;
            visited = new bool[count];
            adjacent = new List<int>[count];
            for (int i = 0; i < count; ++i)
            {
                adjacent[i] = new List<int>();
            }
        }

        public void AddEdge(int v1, int v2)
        {
            adjacent[v1].Add(v2);
        }

        public void BFS(int vCurrent)
        {
            Queue<int> queue = new Queue<int>();
            visited[vCurrent] = true;

            queue.Enqueue(vCurrent);
            while(queue.Count != 0)
            {
                vCurrent = queue.Dequeue();
                Console.WriteLine("->" + vCurrent);

                foreach (var vertex in adjacent[vCurrent])
                {
                    if (!visited[vertex])
                    {
                        visited[vertex] = true;
                        queue.Enqueue(vertex);
                    }
                }
            }
        }

        public void DFS(int vCurrent)
        {
            Stack<int> stack = new Stack<int>();
            visited[vCurrent] = true;

            stack.Push(vCurrent);
            while (stack.Count != 0)
            {
                vCurrent = stack.Pop();
                Console.WriteLine("->" + vCurrent);

                foreach (var vertex in adjacent[vCurrent])
                {
                    if (!visited[vertex])
                    {
                        visited[vertex] = true;
                        stack.Push(vertex);
                    }
                }
            }
        }
    }
}
