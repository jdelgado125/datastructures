﻿using System;
using System.Collections.Generic;
using System.Text;
using Trees.TreeNode;

namespace Trees.BT
{
    public class BinarySearchTree
    {
        public Node _root = null;
        private Node _current = null;

        //Binary Tree is a recurvise data structure
        public BinarySearchTree()
        {
            //Default Constructor
        }

        public BinarySearchTree(int root)
        {
            _root = new Node(root);
        }

        public void Add(int value)
        {
            //Create a new node from the parameter
            _current = new Node(value);

            //Check if the root node is empty
            if(_root == null)
            {
                _root = _current;
                return;
            }

            //Otherwise use recursion to add the node
            Add(_root, value);
        }

        private void Add(Node parent, int value)
        {
            //Check if the root is somehow null
            if (parent == null) return;

            //We have a root node so lets add to the tree
            if(parent.value > value)
            {
                //Our value is smaller than our current root
                //Add to the left node
                if(parent.LeftNode == null)
                {
                    //Left node is empty - set our new node to the left node
                    parent.LeftNode = _current;
                }
                else
                {
                    //Otherwise we traverse down the left side
                    Add(parent.LeftNode, value);
                }
            }
            else
            {
                //Our value is greater than our current root
                //Add to the right node
                if(parent.RightNode == null)
                {
                    //Right node is empty - set our node to the right node
                    parent.RightNode = _current;
                }
                else
                {
                    //Found data - traverse down the right side
                    Add(parent.RightNode, value);
                }
            }
        }

        public void Display()
        {
            Display(_root);
        }

        private void Display(Node root)
        {
            //Left-Node-Right (In-Order) Traversal to Display
            //all the nodes in ascending order
            //Depth-First Algorithm
            //Input: Current root

            //Return if there are no more nodes to traverse
            if(root == null)
            {
                return;
            }

            //Traverse all the way down the left side until you reach a leaf node
            Display(root.LeftNode);

            //Display the current node value
            Console.WriteLine(root.value + " ");

            //Traverse all the way down the right side until you reach a leaf node
            Display(root.RightNode);
        }
    }
}
