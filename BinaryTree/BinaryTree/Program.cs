﻿using System;
using Trees.BT;

namespace Trees
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree BT = new BinarySearchTree();
            BT.Add(25);
            BT.Add(20);
            BT.Add(30);
            BT.Add(15);
            BT.Add(40);
            BT.Add(10);
            BT.Add(35);
            BT.Add(55);
            BT.Display();
        }
    }
}
