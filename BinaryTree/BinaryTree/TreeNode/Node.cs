﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trees.TreeNode
{
    public class Node
    {
        public int value;
        public Node LeftNode = null;
        public Node RightNode = null;

        public Node()
        {
        }

        public Node(int data)
        {
            value = data;
        }
    }
}
