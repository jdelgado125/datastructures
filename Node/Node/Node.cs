﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node
{
    class Node<T>
    {
        public T data;
        public Node<T> Next { get; set; }

        public Node(T data)
        {
            this.data = data;
        }
    }
}
