﻿using System;

namespace Node
{
    class Program
    {
        static void Main(string[] args)
        {
            Node<string> strawberry = new Node<string>("Berry Tasty");
            Node<string> vanilla = new Node<string>("Vanilla");
            Node<string> coconut = new Node<string>("Coconuts for Coconut");
            vanilla.Next = strawberry;
            strawberry.Next = coconut;

            Node<string> currentNode = vanilla;
            while (currentNode != null)
            {
                Console.WriteLine(currentNode.data);
                currentNode = currentNode.Next;
            }
        }
    }
}
